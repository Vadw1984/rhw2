import React, {Component} from 'react';


class ButtonModalHeader extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        // console.log(this.props,'PropsOfButton');
        return (
            <div>
                <button className="button"
                        style={{
                            background: this.props.styleBgButton,
                            borderRadius: this.props.styleBorRadButton,
                            width: this.props.width,
                            height: this.props.height,
                        }}

                        name={this.props.name}
                        onClick={(e)=>{this.props.functionDefindTarget(e);this.props.functionClickState()}}
                >
                    {this.props.textButton}
                </button>
            </div>
        );
    };
}

export default ButtonModalHeader;

