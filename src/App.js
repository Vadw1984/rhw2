import React, { Component } from "react";
import ModalHeader from "./Components/modal/ModalHeader.jsx";
import ButtonModalHeader from "./Components/modal/ButtonModalHeader.jsx";
import Obfuscate from "./Components/modal/Obfuscate";
import GoodsCard from "./Components/GoodsCard";

import "./App.css";

class App extends Component {

  state = {
    isClickedBtn: false,
    headerNameFirstButton: "Add to cart",
    backGroundModal: "",
    backGroundFirstModal: "green"
  };

  clickState = () => {
    this.setState(prevState => ({
      isClickedBtn: !prevState.isClickedBtn
    }));
  };

  defindClickTarget = () => {
    this.headerModalText = "Do you want to add this picture to your gabrbage?";
    this.modalText =
      "Once you delete this file, it won’t be possible to undo this action. \n" +
      "Are you sure you want to delete it?";
    this.backGroundModal = "green";
  };

  render() {
    const { isClickedBtn } = this.state;

    return (
      <div className="App">
        <GoodsCard />

        {isClickedBtn && (
          <Obfuscate
            functionDefindTarget={this.defindClickTarget}
            functionClickState={this.clickState}
          />
        )}

        <ButtonModalHeader
          name="AddToCard"
          functionDefindTarget={this.defindClickTarget}
          functionClickState={this.clickState}
          textButton={this.state.headerNameFirstButton}
          styleBgButton={this.state.backGroundFirstModal}
          marginButton="10px"
          styleBorRadButton="7px"
        ></ButtonModalHeader>

        {isClickedBtn && (
          <ModalHeader
            functionDefindTarget={this.defindClickTarget}
            functionClickState={this.clickState}
            headerModalText={this.headerModalText}
            textModal={this.modalText}
            styleBgModal={this.backGroundModal}
          />
        )}
      </div>
    );
  }
}

export default App;
